import resTab from '../tablesOfStuff/resourcesTab.js';


class Pop {
    constructor(dataInput = {}) {
        //this function and line below make default value 0 if its not given
        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);

        this.popName = data.name;


        //on daily basis
        this.resUsage = new Map();
        this.resUsage.set(resTab.resourceTab.glass, 0.0001);
        this.resUsage.set(resTab.resourceTab.steel, 0.0003);
    }

    insideCall(){
        console.log("im called from pop class and this is my name: " + this.popName);
    }
}

export default {
    Pop: Pop
}