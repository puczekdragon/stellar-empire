import astroObj from './astronomicalObject.js';
import basePops from './basePop.js';
import resTab from '../tablesOfStuff/resourcesTab.js';
import buildingsTab from '../tablesOfStuff/buildings.js';
import refreshPlanetStatsExtFun from '../supperFunctions/refreshPlanetStats.js'


class Planet extends astroObj.AstronomicalObject {
    constructor(dataInput = {}) {
        //this function and line below make default value 0 if its not given
        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);

        console.log(dataInput);
        super(dataInput);
        this.averangeTemp = data.averangeTemp;

        this.corruption = data.corruption;

        this.population = data.population;
        this.populationType = new basePops.Pop({
            name: "Base Pop"
        });

        //default rates, given in yearly ticks
        this.basePopGrowth = 0.001;
        this.basePopDecedline = 0.00075;


        //initializint stockpile as a map and giving it some starting numbers
        this.stockpile = new Map();
        this.stockpile.set(resTab.resourceTab.glass.resSimpleName, 100);
        this.stockpile.set(resTab.resourceTab.steel.resSimpleName, 1000);
        this.stockpile.set(resTab.resourceTab.plastic.resSimpleName, 0);

        //initialising empty building table
        this.buildings = [];

    }



    refreshPlanetStats() {
        refreshPlanetStatsExtFun(this);
    };

    returnStockpile() {
        let retVal = "Stockpile of planet " + this.objName + ": \n";
        this.stockpile.forEach((values, keys) => {
            retVal += keys + ": " + values + "\n";
        });
        return retVal;
    }

    addBuilding(newBuilding, ifFree = 0) {

        //those vars are added to use them inside functions messing up "this" keyword
        let tempBuildings = this.buildings,
            ifAdded = false,
            planet = this;


        // TODO add any other building types then common buildings

        buildingsTab.commonBuildings.forEach(e => {
            if (e.buildingSimpleName == newBuilding && !ifAdded) {
                console.log(e);
                //need to run this function before adding any building!
                e.createBuilding({
                    planetLocation: planet
                });
                tempBuildings.push(e);
                ifAdded = true;
            }
        })
        this.buildings = tempBuildings;
    }
}


export default {
    Planet: Planet
}