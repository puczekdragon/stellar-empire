class AstronomicalObject {
    constructor(dataInput) {


        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);

        console.log(dataInput);


        //all units are SI
        this.radious = data.radious;
        this.objName = data.objName;
        this.locX = data.locX;
        this.locY = data.locY;
        this.locZ = data.locZ;
        this.objClass = data.objClass;
    }
}

export default {
    AstronomicalObject: AstronomicalObject
}