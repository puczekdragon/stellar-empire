import resTab from '../tablesOfStuff/resourcesTab.js';

class Building {

    constructor(dataInput = {}) {
        //this function and line below make default value 0 if its not given
        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);


        this.buildingName = data.name;
        //replace all spaces to underscore, all to alphanumerlical, all to lowercase
        this.buildingSimpleName = data.name.replace(/ /g, "_").replace(/\W/g, '').toLowerCase();
        this.buildingSize = data.size;
        this.buildingClasses = data.classes;
        this.resUpkeep = data.resUpkeep;

        //tells how much of resources was provided for upkeep, 0 -> none, 0.5 -> 50%, 1 -> 100% etc.
        let upkeepResProvided = 0;

    }

    //function to run when creating new building
    createBuilding(dataInput = {}) {
        //this function and line below make default value 0 if its not given
        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);

        //planet where building is
        this.planetLocation = data.planetLocation;
    }

    //return stockpile after one day of upkeep
    upkeepCostForOneDay(stockpile) {
        if (this.resUpkeep) {

            this.resUpkeep.forEach(res => {
                let tempValue = stockpile.get(res.res.resSimpleName);
                //check if there is enough resources to sattisfy upkeep demand
                if(tempValue - res.quant > 0) {
                    tempValue = tempValue - res.quant;
                    stockpile.set(res.res.resSimpleName, tempValue);
                    this.upkeepResProvided = 1;
                }
                else {
                    this.upkeepResProvided = 0;
                }
            });
        }
        return stockpile;
    }

}

class CommonBuilding extends Building {
    constructor(dataInput = {}) {
        //this function and line below make default value 0 if its not given
        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);
        super(dataInput);

        this.resOutput = data.resOutput;
        this.resInput = data.resInput;

        //tells how much of resources was provided for input, 0 -> none, 0.5 -> 50%, 1 -> 100% etc.
        let inputResProvided = 0;


    }

    //return stockpile after one day of work, calculate input only
    inputForOneDay(stockpile) {
        if (this.resInput) {
            this.resInput.forEach(res => {

                //multiplayed by uppkeep
                let resUsed = stockpile.get(res.res.resSimpleName) - (res.quant * this.upkeepResProvided);

                //TODO: multiple input
                //if there is not enought res to sattisfy input
                if (resUsed > 0) {
                    stockpile.set(res.res.resSimpleName, resUsed);
                    this.inputResProvided = 1;
                } else {
                    this.inputResProvided = 0;
                    console.warn("No resources for: " + this.buildingName + ", on planet: " + this.planetLocation.objName);
                }
            });
        }
        return stockpile;
    }

    //return stockpile after one day of work, calculate output only
    outputForOneDay(stockpile) {
        if (this.resOutput) {
            this.resOutput.forEach(res => {
                let tempValue = stockpile.get(res.res.resSimpleName);

                //its multiplayed by % of how much input and upkeep is provided, when there is no resource output become 0
                tempValue = tempValue + (res.quant * this.inputResProvided * this.upkeepResProvided);
                stockpile.set(res.res.resSimpleName, tempValue);
            });
        }
        return stockpile;
    }

}


export default {
    Building: Building,
    CommonBuilding: CommonBuilding
}