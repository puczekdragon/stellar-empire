class Resource {

    constructor(dataInput = {}) {
        //this function and line below make default value 0 if its not given
        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);

        this.resName = data.name;
        //replace all spaces to underscore, all to alphanumerlical, all to lowercase
        this.resSimpleName = data.name.replace(/ /g,"_").replace(/\W/g, '').toLowerCase();

        this.resvalue = data.value;
    }

}

class Steel extends Resource {
    constructor(dataInput = {}) {
        //this function and line below make default value 0 if its not given
        super(dataInput);
        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);

    }
}

class Glass extends Resource {
    constructor(dataInput = {}) {
        //this function and line below make default value 0 if its not given
        super(dataInput);
        var defValue = {
            get: function (target, name) {
                return target.hasOwnProperty(name) ? target[name] : 0;
            }
        };
        let data = new Proxy(dataInput, defValue);

    }
}


export default {
    Resource: Resource,
    Steel: Steel,
    Glass: Glass
}