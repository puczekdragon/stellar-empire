import buildings from '../supperClasses/building.js';
import resourcesTab from './resourcesTab.js'


const steel_workshop = new buildings.CommonBuilding({
    name: "Steel workshop",
    size: 1,
    classes: ["manufactory", "simple", "steel"],
    resUpkeep: [
        {
            res: resourcesTab.resourceTab.steel,
            quant: 10
        },
        {
            res: resourcesTab.resourceTab.glass,
            quant: 2
        }
    ]
});
const glass_workshop = new buildings.CommonBuilding({
    name: "Glass workshop",
    size: 1,
    classes: ["manufactory", "simple", "glass"],
    resUpkeep: [
        {
            res: resourcesTab.resourceTab.steel,
            quant: 15
        },
        {
            res: resourcesTab.resourceTab.glass,
            quant: 1
        }
    ]
});

const plasticFactory = new buildings.CommonBuilding({
    name: "Plastic factory",
    size: 1,
    classes: ["factory", "common", "plastic"],
    resUpkeep: [
        {
            res: resourcesTab.resourceTab.steel,
            quant: 3
        }
    ],
    resInput: [
        {
            res: resourcesTab.resourceTab.glass,
            quant: 10
        }
    ],
    resOutput: [
        {
            res: resourcesTab.resourceTab.plastic,
            quant: 7.5
        }
    ]
});
const commonBuildings = [
    steel_workshop,
    glass_workshop,
    plasticFactory
]

export default {

    commonBuildings: commonBuildings
}