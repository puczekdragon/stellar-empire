import resources from '../supperClasses/resources.js';

const glass = new resources.Resource({
    name: "Glass",
    value: 11
});
const steel = new resources.Resource({
    name: "Steel",
    value: 5
});
const plastic = new resources.Resource({
    name: "Plastic",
    value: 3
});

// TODO change it to array
const resourceTab = {
    glass,
    steel,
    plastic
}

export default {

    resourceTab: resourceTab
}