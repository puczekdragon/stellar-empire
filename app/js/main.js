import $ from 'jquery';
import planets from './supperClasses/planets.js';
import hamsters from 'hamsters.js'

hamsters.init({
    maxThreads: 64
});



let teest = new planets.Planet({
    averangeTemp: 34,
    objName: "Orenta-4",
    population: 123456
});

console.log("test planet population: " + teest.populationType.popName);
teest.populationType.insideCall();


function mainGameLoop(mainLoopRuns = 0) {
    console.log("One game loop run");



    if(mainLoopRuns < 2) teest.addBuilding("steel_workshop");
    if(mainLoopRuns < 2) teest.addBuilding("glass_workshop");
    if(mainLoopRuns < 1) teest.addBuilding("plastic_factory");
    

    teest.refreshPlanetStats();




    let miniUI = document.getElementById('push-into-me');
    if(!miniUI) {
        miniUI = document.createElement("div");
    }
    miniUI.innerHTML = " ";
    miniUI.innerHTML += (teest.objName + " planet population: " + teest.population + "<br>");

    //replacing newlines with br for use in innerHTML
    miniUI.innerHTML += (teest.returnStockpile().replace(/(?:\r\n|\r|\n)/g, '<br>'));




    //limit to 20 runs to not overwork my poor PC
    mainLoopRuns++;
    if (mainLoopRuns < 10) {
        setTimeout(function () {
            mainGameLoop(mainLoopRuns);
        }, 500);
    }
}
mainGameLoop();