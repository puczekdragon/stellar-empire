//planet argument must be "this" from class
export default function refreshPlanetStats(planet) {
    

    //population growth
    planet.population = parseInt(planet.population + planet.population * planet.basePopGrowth - planet.population * planet.basePopDecedline);

    //it must be, to omit this use inside foreaches
    let tempStockpile = planet.stockpile;
    //pops using resources
    tempStockpile.forEach(function (value, key, map) {
        let tempValue = value - this.population * this.populationType.resUsage.get(key);
        // map.set(key, tempValue);
    }, planet);

    //building upkeep
    planet.buildings.forEach(building => {
        tempStockpile = building.upkeepCostForOneDay(tempStockpile);
    });


    //building input
    planet.buildings.forEach(building => {
        tempStockpile = building.inputForOneDay(tempStockpile);
    });
    //building output
    planet.buildings.forEach(building => {
        tempStockpile = building.outputForOneDay(tempStockpile);
    });

    


    planet.stockpile = tempStockpile;
};
